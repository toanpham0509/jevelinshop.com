<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pushmetop.com_test' );

/** MySQL database username */
define( 'DB_USER', 'dev' );

/** MySQL database password */
define( 'DB_PASSWORD', 'dev' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'F>5033SjBuYB,Roo;bTQ3-?IE,u@@jTy&sv/SW|>xcr U>hH9/F/5,y=JMXpFBm2' );
define( 'SECURE_AUTH_KEY',  '#&9{NLvUPd@VSx)+w{Brw=C}QxbEOw.F:0aGb%^*W6mdwQiZzP@<`p_.>pBsbT8E' );
define( 'LOGGED_IN_KEY',    '#)e)i~#ex8!*_+CY$s(jWA/]wkH1X}D+W3F$a:+8;2to5iJ6[92]e7Yjq@~8q8Xt' );
define( 'NONCE_KEY',        'Y;!Z;1H<U#U-phI@H/(;/x}cRa^QDT?54Ps01f~FX9^E:1KRQZie-*y7s9iX{10%' );
define( 'AUTH_SALT',        'o.&2_NhdxvVSTa+dy:5j:IH@):SJ?3&R(:FZY,85n<3V-9*8&8uP=T/`;AHw8uuO' );
define( 'SECURE_AUTH_SALT', 'n)@p/T8v5Lwtd:3 L9+kQ?2^ha_uTQ)-pk5HDp^J]8p@A/t3UYhps&?Ya]$,cz.`' );
define( 'LOGGED_IN_SALT',   'eDa/$uwpnNHAf6P/EAf[$#&ZU9*qd1U_><VptBqGY+)sO5ZF8?2G;FUN {|qqXNE' );
define( 'NONCE_SALT',       '1bm;B&:;-2BCprm4D5+2Egy}M3Mv[Nw4aPojQmWdCSa3(cSr0po+;&lWeaeCLhR{' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
